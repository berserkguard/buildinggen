# BuildingGen: Procedural 3D Building Generator

### About

BuildingGen is being developed as part of my senior research project. I chose this as my project because I wanted to learn how procedural generation works and I also wanted to learn Blender's Python API. There are currently three types of rulesets: castle ruleset, house ruleset, and table ruleset. Feel free to make your own rulesets!

### How to Use
Developed for Blender v2.69 with Python 3 - Not tested on other versions!

1. Copy 'src' folder to 'C:\Program Files\Blender Foundation\Blender\2.69\scripts\addons' and rename to 'buildinggen'.

2. (optional) Create any desired rulesets and place in the 'rulesets' folder in the addon's directory.

3. Launch Blender and enable add-on from within user preferences.

4. From within Blender, call the "BuildingGen" operator (bpy.ops.object.buildinggen).

5. Choose the desired ruleset and watch as a building auto-magically appears!