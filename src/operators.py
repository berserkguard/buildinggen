import bpy
import math
import os

from buildinggen.constants import *
from buildinggen.util import *

class Operators:
    # Returns the input object.
    @staticmethod
    def get_current():
        return [bpy.context.scene.objects.active]
    
    # Places a plane mesh at the specified position.
    @staticmethod
    def place_plane(pos):
        Util.setup_operation('OBJECT')
        
        parent_obj = bpy.context.scene.objects.active
        
        # Make sure nothing's selected initially.
        for ob in bpy.context.scene.objects:
            if ob.type == 'MESH':
                ob.select = False
        
        # Place the mesh
        bpy.ops.mesh.primitive_plane_add(view_align=False, enter_editmode=False, location=pos)
        
        new_obj = bpy.context.selected_objects[0]

        # Set the placed mesh's parent to the predecessor.
        if parent_obj is not None and new_obj is not None:
            new_obj.parent = parent_obj
                
        # Need to reset the object's location because of the parenting.
        new_obj.location = pos
        
        return [new_obj]

    # Removes the active object from the scene.
    # FIXME: This crashes :P
    @staticmethod
    def remove():
        Util.setup_operation('OBJECT')
        
        bpy.ops.object.delete(use_global=False)
        
        # Return an empty list - no mesh to assign label to!
        return []

    # Places an *.obj mesh at the current position.
    # Parameter 'file' is a relative path to the *.obj file to place.
    @staticmethod
    def place_mesh(file):
        Util.setup_operation('OBJECT')
        
        parent_obj = bpy.context.scene.objects.active
        
        # Make sure nothing's selected initially.
        for ob in bpy.context.scene.objects:
            if ob.type == 'MESH':
                ob.select = False
        
        # Place the obj
        bpy.ops.import_scene.obj(filepath=file)
        
        new_obj = bpy.context.selected_objects[0]
        
        # Set the placed mesh's parent to the predecessor.
        if parent_obj is not None and new_obj is not None:
            new_obj.parent = parent_obj
        
        bpy.ops.object.scale_clear()
        bpy.ops.object.rotation_clear()
        bpy.ops.object.location_clear()
        
        # TODO: may need to update this if above line is uncommented!
        return [new_obj]

    @staticmethod
    def apply_texture(tex_path):
        Util.setup_operation('OBJECT')

        bpy.ops.object.material_slot_remove()
        
        img = bpy.data.images.load(tex_path)
        ob = bpy.context.scene.objects.active
        
        #bpy.ops.object.material_slot_add()
        mat = bpy.data.materials.new("NewMat")
        ob.data.materials.append(mat)
        
        for tex in ob.data.uv_textures:
            for data in tex.data:
                data.image = img
        
        for f in ob.data.polygons:
            f.material_index = 0
        
        #bpy.context.scene.objects.active.active_material = mat
        
        return []
        
    # Applies cube projection to the selected object.
    @staticmethod
    def uv_project_cube():
        Util.setup_operation('EDIT')
        
        bpy.ops.uv.unwrap()
        
        bpy.ops.uv.select_all(action='SELECT')
        bpy.ops.uv.cube_project()
        
        bpy.ops.object.mode_set(mode='OBJECT', toggle=False)
        
        return []

    # Scales the selected object by the specified amounts along each axis.
    @staticmethod
    def scale(x, y, z):
        Util.setup_operation('OBJECT')

        bpy.ops.transform.resize(value=(x, x, x), constraint_axis=(True, False, False), constraint_orientation='GLOBAL', mirror=False, remove_on_cancel=True)
        bpy.ops.transform.resize(value=(y, y, y), constraint_axis=(False, True, False), constraint_orientation='GLOBAL', mirror=False, remove_on_cancel=True)
        bpy.ops.transform.resize(value=(z, z, z), constraint_axis=(False, False, True), constraint_orientation='GLOBAL', mirror=False, remove_on_cancel=True)
        
        bpy.ops.object.transform_apply(scale=True)
        
        return [bpy.context.scene.objects.active]
    
    # Translates the selected object by the specified amounts (global X, Y, Z).
    # Parameter must be a 3-tuple holding the values to use for translation (X, Y, Z).
    @staticmethod
    def translate(amts):
        Util.setup_operation('EDIT')
        bpy.ops.mesh.select_all(action='SELECT')
        
        bpy.ops.transform.translate(value=amts, constraint_axis=(False, False, False), constraint_orientation='GLOBAL', mirror=False)
        
        # Apply transform so future children don't inherit it!
        #bpy.ops.object.transform_apply(location=True, rotation=False, scale=False)
        
        return [bpy.context.scene.objects.active]
    
    # Rotates the selected object around the given global axis by the specified amount (degrees).
    # Axis can be one of 'X', 'Y', or 'Z', and is global. Z by default.
    @staticmethod
    def rotate(axis, amt):
        Util.setup_operation('EDIT')
        bpy.ops.mesh.select_all(action='SELECT')
        
        val = amt * math.pi / 180.0
        
        axis_vec = (0, 0, 1)
        const_vec = (False, False, True)
        if axis == 'X':
            axis_vec = (1, 0, 0)
            const_vec = (True, False, False)
        elif axis == 'Y':
            axis_vec = (0, 1, 0)
            const_vec = (False, True, False)
        
        bpy.ops.transform.rotate(value=val, axis=axis_vec, constraint_axis=const_vec, constraint_orientation='GLOBAL')
        
        # Apply transform so future children don't inherit it!
        #bpy.ops.object.transform_apply(location=False, rotation=True, scale=False)
        
        return [bpy.context.scene.objects.active]
    
    # Extrudes the selected object along its common normal.
    @staticmethod
    def extrude(amt):
        Util.setup_operation('EDIT')
        bpy.ops.mesh.select_all(action='SELECT')

        # Get the common normal of the selected polys, so we can extrude along it
        val = Util.get_common_normal() * amt
        bpy.ops.mesh.extrude_region_move(TRANSFORM_OT_translate={"value": val, "constraint_orientation": "NORMAL"})

        # Make normals consistent
        bpy.ops.mesh.normals_make_consistent(inside=False)
        
        return [bpy.context.scene.objects.active]

    # Extrudes & componentizes the selected object along the given axis by the specified amount.
    # Axis can be one of 'X', 'Y', or 'Z', and is local to object.
    @staticmethod
    def component_extrude(axis, amt):
        Util.setup_operation('EDIT')
        bpy.ops.mesh.select_all(action='SELECT')
        
        val = None
        if axis == "X":
            val = (amt, 0, 0)
        elif axis == "Y":
            val = (0, amt, 0)
        elif axis == "Z":
            val = (0, 0, amt)

        floor = bpy.context.scene.objects.active
        
        bpy.ops.mesh.duplicate_move(TRANSFORM_OT_translate={"value": val, "constraint_orientation": "LOCAL"})
        
        roof = Util.separate_object()
        
        bpy.ops.object.mode_set(mode='OBJECT', toggle=False)
        
        bpy.context.scene.objects.active = floor
        
        bpy.ops.object.mode_set(mode='EDIT', toggle=False)
        bpy.ops.mesh.select_all(action='SELECT')
        
        bpy.ops.mesh.region_to_loop()
        bpy.ops.mesh.duplicate()
        
        walls = Util.separate_object()
                
        bpy.ops.object.mode_set(mode='OBJECT', toggle=False)
        
        bpy.context.scene.objects.active = walls

        bpy.ops.object.mode_set(mode='EDIT', toggle=False)
        bpy.ops.mesh.select_all(action='SELECT')
        
        if val is not None:
            bpy.ops.mesh.extrude_region_move(TRANSFORM_OT_translate={"value": val, "constraint_orientation": "LOCAL"})
        
        bpy.ops.object.mode_set(mode='OBJECT', toggle=False)
        
        # Need to make wall normals consistent, for some reason it only works at the end of all the other operations.
        bpy.context.scene.objects.active = walls
        bpy.ops.object.mode_set(mode='EDIT', toggle=False)
        bpy.ops.mesh.select_all(action='SELECT')
        
        bpy.ops.mesh.normals_make_consistent(inside=False)

        bpy.ops.object.mode_set(mode='OBJECT', toggle=False)
        
        return [floor, walls, roof]