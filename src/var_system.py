import bpy
import random

class RulesetVar:
    count = 0 # Used for variable ordering on the UI.
    
    def __init__(self, id):
        # The type of widget to use in the RulesetConfigurator dialog.
        self.widget_type = None
        
        self.id = id
        
        self.index = RulesetVar.count
        RulesetVar.count += 1
        
        # The final computed value of this variable.
        self.val = None
    
    # Called by Engine to get a new value.
    def recalculate(self):
        pass
    
    # Called by the Engine to make sure the variable has valid settings.
    def clean_up(self):
        pass
    
    # Returns a bpy.props object representing the given variable.
    def get_prop(self, idx):
        return None
    
class IntegerVar(RulesetVar):
    def __init__(self, id):
        RulesetVar.__init__(self, id)
        
        # The type of integer var. Can be "Fixed" or "Random"
        self.type = None
        
        self.min = None
        self.max = None
        
        self.user_min = None
        self.user_max = None
        
        self.step = 1
    
    def set_min(self, val):
        self.min = val
        self.user_min = val
        
    def set_max(self, val):
        self.max = val
        self.user_max = val
    
    def set_range(self, min, max):
        self.set_min(min)
        self.set_max(max)
    
    def recalculate(self):
        if self.type == "Random":
            self.val = random.randrange(self.user_min, self.user_max + 1, self.step)
    
    def clean_up(self):
        if self.user_min is not None and self.user_max is not None:
            if self.user_min > self.user_max:
                tmp = self.user_min
                self.user_min = self.user_max
                self.user_max = tmp
                #if self.user_min < self.min:
                #    self.user_min = self.min
                #if self.user_max > self.max:
                #    self.user_max = self.max
    
    def get_prop(self, idx):
        def get_fixed(s): return self.val
        def set_fixed(s, val): self.val = val

        def get_min(s): return self.user_min
        def set_min(s, val): self.user_min = val
        def get_max(s): return self.user_max
        def set_max(s, val): self.user_max = val
            
        if self.type == "Fixed":
            if idx == 0:
                return bpy.props.IntProperty(name=self.id, default=self.val, min=self.min, max=self.max, get=get_fixed, set=set_fixed)
        elif self.type == "Random":
            if idx == 0:
                return bpy.props.IntProperty(name=self.id+".min", default=self.min, min=self.min, max=self.max, step=self.step, get=get_min, set=set_min)
            elif idx == 1:
                return bpy.props.IntProperty(name=self.id+".max", default=self.max, min=self.min, max=self.max, step=self.step, get=get_max, set=set_max)
        return None

class EnumVar(RulesetVar):
    def __init__(self, id):
        RulesetVar.__init__(self, id)
        
        # The enum items
        self.items = []
        
        # The index of the selected enum.
        self.val_idx = None
    
    def add_item(self, name, description):
        self.items.append((name, name, description))
        if len(self.items) == 1:
            self.val_idx = 0
            self.val = name
        
    def recalculate(self):
        pass
        
    def clean_up(self):
        pass
    
    def get_prop(self, idx):
        def get_enum(s):
            return self.val_idx
        def set_enum(s, val):
            self.val = self.items[val][0]
            self.val_idx = val
            
        if idx == 0:
            return bpy.props.EnumProperty(items=self.items, name=self.id, get=get_enum, set=set_enum)
        return None

class BoolVar(RulesetVar):
    def __init__(self, id):
        RulesetVar.__init__(self, id)
        
        self.val = False
    
    def get_prop(self, idx):
        def get_bool(s):
            return self.val
        def set_bool(s, val):
            self.val = val
        
        if idx == 0:
            return bpy.props.BoolProperty(name=self.id, get=get_bool, set=set_bool)
        return None

# Provides different types of variables for rulesets which may be overridden via the Configure Ruleset dialog.
class VarSystem:
    def __init__(self):
        random.seed()
        
        self.vars = {}
    
    def reset(self):
        self.__init__()
    
    def get(self, id):
        var = self.vars[id]
        if var.val is None:
            var.recalculate()
        return var.val

    def recalculate_all(self):
        for id, var in self.vars.items():
            var.recalculate()
    
    def clean_up_all(self):
        for id, var in self.vars.items():
            var.clean_up()
    
    # TODO add step support
    def new_integer(self, id, val=None, min=None, max=None, step=None):
        intvar = IntegerVar(id)
        
        if val is None:
            if min is None and max is None:
                val = 0
                intvar.type = "Fixed"
                intvar.val = val
            elif min is not None and max is not None:
                intvar.type = "Random"
                intvar.set_min(min)
                intvar.set_max(max)
        else:
            intvar.type = "Fixed"
            intvar.val = val
            intvar.set_min(min)
            intvar.set_max(max)
        
        # Add the new variable to the list.
        self.vars[id] = intvar
    
    # Items should be a list of (name, description).
    def new_enum(self, id, items):
        enumvar = EnumVar(id)
        
        for item in items:
            enumvar.add_item(item[0], item[1])
        
        self.vars[id] = enumvar
    
    def new_bool(self, id, val=False):
        boolvar = BoolVar(id)
        
        boolvar.val = val
        
        # Add the new variable to the list.
        self.vars[id] = boolvar