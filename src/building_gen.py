import bpy
import mathutils
import sys

# BuildingGen imports
from buildinggen.util import *
from buildinggen.ruleset import *
from buildinggen.operators import *
from buildinggen.constants import *
from buildinggen.engine import *

# Make sure we can load rulesets from the "rulesets/" directory.
sys.path.append(Constants.RULESET_PATH)

# Class for packaging BuildingGen as a Blender add-on.
class BuildingGen(bpy.types.Operator):
    """Generates 3D building variations according to defined rulesets."""
    bl_idname = "object.building_gen"	# unique identifier for buttons and menu items to reference.
    bl_label = "BuildingGen"			# display name in the interface.
    bl_options = {'REGISTER', 'UNDO'}	# enable undo for the operator.

    # See ui.py for details on how the ruleset_module is set.
    # Can modify ruleset_module in real-time by running the "BuildingGen - Set Ruleset" operation.
    ruleset_module = ""
    
    # The instance of the dynamically-loaded Ruleset.
    ruleset = None
    
    # Set to true before opening RulesetChooserOperator to tell it to apply the ruleset.
    apply_after_set = False
    
    # Loads the ruleset with the given name.
    @staticmethod
    def load_ruleset(ruleset):
        # Before importing ruleset, reset variable system.
        Engine.vars.reset()
    
        imported_ruleset = __import__(ruleset)
        make_ruleset = getattr(imported_ruleset, ruleset)

        BuildingGen.ruleset = make_ruleset()
        Util.log("Ruleset loaded: " + BuildingGen.ruleset.get_name())
    
    # Applies an already-loaded ruleset.
    @staticmethod
    def apply_ruleset():
        # Recalculate ruleset variables.
        Engine.vars.recalculate_all()
    
        bpy.ops.object.select_all(action='DESELECT')
        
        # If there's already a BuildingGen object, delete it (and it's children!)
        base_obj = bpy.context.scene.objects.get("BuildingGen")
        if base_obj is not None:
            # Mark the container for deletion
            base_obj.select = True
            
            # And, mark its children for deletion too
            children = Util.get_children(base_obj)
            for child in children:
                child.select = True
            
            # Now, delete them all!
            bpy.ops.object.delete(use_global=False)
        
        # Add a base container object named BuildingGen
        bpy.ops.object.empty_add(type='PLAIN_AXES', view_align=False, location=(0, 0, 0))
        
        bpy.context.active_object.name = "BuildingGen"
        
        #Operators.place_plane((0, 0, 0))
        
        # Apply the ruleset
        BuildingGen.ruleset.apply()
        
        bpy.ops.object.mode_set(mode='OBJECT', toggle=False)
        bpy.ops.object.select_all(action='DESELECT')
        
        Util.render_setup()
    
    # Called by Blender when running the operator.
    def execute(self, context):
        Util.log("Executing...")
        
        if BuildingGen.ruleset is None:
            # If no ruleset is loaded, show the ruleset chooser dialog.
            BuildingGen.apply_after_set = True
            bpy.ops.object.buildinggen_ruleset_chooser('INVOKE_DEFAULT')
        else:
            # Otherwise, apply the ruleset immediately.
            BuildingGen.apply_ruleset()
        
        Util.log("Execution completed.")
        
        # Let Blender know the operator finished successfully.
        return {'FINISHED'}
