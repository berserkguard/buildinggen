# A class for keeping track of widely-used constants.
class Constants:
    METADATA_PREFIX = "bgen_"
    RULESET_PATH = "rulesets"