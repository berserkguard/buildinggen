from buildinggen.util import *

# Class for handling command-line arguments.
class CommandOptions:
    # Holds information about options.
    options = {}

    # Currently set values (defaults + user overrides)
    values = {}

    @staticmethod
    def add_option(switch, num_args, default_values, required, help_str):
        CommandOptions.options[switch] = (
            switch,
            num_args,
            default_values,
            required,
            help_str
        )
    
    @staticmethod
    def is_option(arg):
        return arg in CommandOptions.options.keys()

    @staticmethod
    def setup_args(argv):
        # Get command line arguments (ex: blender.exe -b test.blend -P __init__.py -- arg1 arg2 ...)
        # Above example will have argv[0] = "arg1" and argv[1] = "arg2"
        if "--" in argv:
            argv = argv[argv.index("--") + 1:]
        else:
            argv = []

        # If --help is passed as the only arg, display usage string & return.
        if len(argv) == 1 and argv[0] == "--help":
            usage_str = "Usage: blender.exe -b input.blend -P __init__.py --"
            for arg, opt in CommandOptions.options.items():
                usage_str += " " + opt[4]
        
            print("")
            print(usage_str)
            print("")
            
            return False
        
        # First, make sure we initialize everything to default values.
        CommandOptions.values = {}
        req_args_set = {}
        for arg, opt in CommandOptions.options.items():
            CommandOptions.values[opt[0]] = opt[2]
            
            # Also keep track of whether or not required args are set.
            req_args_set[opt[0]] = not opt[3]
        
        # Get all options and their passed values (and perform basic error-checking).
        cur_option = None
        next_switch = 0
        opt_vals = []
        for arg_idx in range(len(argv)):
            arg = argv[arg_idx]
            if next_switch == arg_idx:
                if CommandOptions.is_option(arg):
                    if cur_option is not None:
                        CommandOptions.values[cur_option] = opt_vals
                        req_args_set[cur_option] = True

                    opt_vals = []
                    next_switch += CommandOptions.options[arg][1] + 1
                    cur_option = arg
                else:
                    Util.log("Error! Invalid command-line option supplied: " + arg)
                    Util.log("Aborting command-line execution...")
                    return False
            else:
                opt_vals.append(arg)
        
        # Gotta make sure to set the values for the last option!
        if cur_option is not None:
            CommandOptions.values[cur_option] = opt_vals
            req_args_set[cur_option] = True
        
        # Ensure all required arguments were specified
        for opt, arg_set in req_args_set.items():
            if not arg_set:
                Util.log("Error! Required argument missing: " + opt)
                Util.log("Aborting command-line execution...")
                return False
        
        # No errors
        return True

# Ruleset option
CommandOptions.add_option("-r", 1, [None], True, "-r <ruleset_name>")
        
# Output option
CommandOptions.add_option("-o", 1, ["buildinggen_output.blend"], False, "[-o <output_file>]")
