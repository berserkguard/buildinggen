import bpy
import mathutils
import math

from buildinggen.util import *
from buildinggen.operators import *
from buildinggen.constants import *
from buildinggen.var_system import *

import pdb
# pdb.set_trace() to drop to pdb

class Engine:
    global_offset = mathutils.Vector((0.0, 0.0, 0.0))
    global_scale = mathutils.Vector((1.0, 1.0, 1.0))
    global_rotation = mathutils.Vector((0.0, 0.0, 0.0))
    global_metadata = dict()
    
    vars = VarSystem()

    # Returns a function which translates the engine's current position by (x, y, z).
    # Mainly used by compound rules. 
    @staticmethod
    def queue_translate(x, y, z):
        def translate():
            Engine.global_offset += mathutils.Vector((x, y, z))
        return translate
    
    # Returns a function which rotates the engine's current object around its local X, Y, Z axes.
    # Mainly used by compound rules.
    @staticmethod
    def queue_rotate(x, y, z):
        def rotate():
            Engine.global_rotation.x += x
            Engine.global_rotation.y += y
            Engine.global_rotation.z += z
        return rotate
    
    # Returns a function which scales the engine's current object.
    # Mainly used by compound rules.
    @staticmethod
    def queue_scale(x, y, z):
        def scale():
            Engine.global_scale.x *= x
            Engine.global_scale.y *= y
            Engine.global_scale.z *= z
        return scale
    
    @staticmethod
    def queue_set_metadata(key, value):
        def set_metadata():
            # Don't prefix key here - prefix is applied in Util.set_metadata
            #key = Constants.METADATA_PREFIX + key
            if key in Engine.global_metadata:
                print("Old value: ", Engine.global_metadata[key])
            Engine.global_metadata[key] = value
            print("New value: ", Engine.global_metadata[key])
        return set_metadata
        
    @staticmethod
    def queue_clear_metadata(key):
        def clear_metadata():
            # Don't prefix key here - prefix is applied in Util.set_metadata
            #key = Constants.METADATA_PREFIX + key
            Engine.global_metadata.pop(key, None)
        return clear_metadata
    
    # Applies a compound operation to all objects which match label.
    @staticmethod
    def apply_compound(label, op_list, test_func = None):
        matched_objects = Engine.get_matches(label, test_func)
        
        # Apply the operation on all the matched objects.
        for object in matched_objects:
            # Apply each operation in the list
            for op in op_list:
                if not isinstance(op, list):
                    # If op isn't a list, it's just a non-mesh operation (engine translation, scale, etc.).
                    op()
                else:
                    # Otherwise, we're doing a mesh operation (first element is new_labels, second element is the func).
                    new_labels = op[0]
                    func = op[1]
                    
                    Engine.apply_object(object, new_labels, func)

    # Returns a list of objects in the scene filtered by matches on label, test_func
    # Candidate objects must be children of a container object named "BuildingGen".
    @staticmethod
    def get_matches(label, test_func = None):
        matched_objects = []
        
        base_obj = bpy.context.scene.objects.get("BuildingGen")
        
        for object in Util.get_children(base_obj):
            # Make sure we only have mesh objects
            if not object.type == 'MESH':
                continue
            
            # Check for a match on label
            if not Util.name_matches(object.name, label):
                continue
            
            # Check for a match on test_func (if it was supplied)
            if test_func is not None:
                if not test_func(object):
                    continue
            
            # Matched on all criteria - add to list!
            matched_objects.append(object)
        
        return matched_objects
    
    @staticmethod
    def set_axiom(label, func):
        base_obj = bpy.context.scene.objects.get("BuildingGen")
        Engine.apply_object(base_obj, label, func)
    
    # Applies the func closure to all objects which match label, giving it new_label(s).
    # If test_func is specified, will only apply the operation if the test_func is true.
    @staticmethod
    def apply(label, new_labels, func, test_func = None):
        bpy.ops.object.mode_set(mode='OBJECT', toggle=False)
        bpy.ops.object.select_all(action='DESELECT')
        
        matched_objects = Engine.get_matches(label, test_func)
        
        # Apply the operation on all the matched objects.
        for object in matched_objects:
            Engine.apply_object(object, new_labels, func)

    # Applies the given operation to the given object.
    @staticmethod
    def apply_object(object, new_labels, func):
        # If only one new label, wrap it in a list for convenience.
        if not isinstance(new_labels, list):
            new_labels = [new_labels]
        
        # We've found a candidate mesh - select & apply function!
        bpy.context.scene.objects.active = object
        
        # Move cursor to be at object's origin (+ global offset)
        #bpy.ops.view3d.snap_cursor_to_active()
        
        # TODO: rotation
        cur_rotation = Engine.global_rotation # * object.rotation_quaternion
        
        modified_objects = func()
        
        # Assign new labels to each of the modified objects
        for modified_object, new_label in zip(modified_objects, new_labels):
            modified_object.name = new_label
            
            # Add any queued up metadata
            for key, value in Engine.global_metadata.items():
                print("METADATA: ", key, " => ", value)
                Util.set_metadata(modified_object, key, value)
            
            # If the modified_object is a child of the parent, don't include the parent's location in calculations!
            if modified_object.parent == object:
                bpy.context.scene.cursor_location = Engine.global_offset
            else:
                bpy.context.scene.cursor_location = object.location + Engine.global_offset

            # Translate each object based on above mode selection.
            modified_object.location = bpy.context.scene.cursor_location
            
            """
            # Convert local -> global coords
            v = modified_object.location
            print ("Local: ", v)
            mat = modified_object.matrix_world

            # Multiply matrix by vertex
            loc = mat * v
            print ("World: ", loc)
            """

            Engine.global_offset = mathutils.Vector((0.0, 0.0, 0.0))
            
            # Rotate each object to the predecessor's rotation + global engine rotation
            modified_object.rotation_mode = 'XYZ'
            modified_object.rotation_euler[0] += math.radians(cur_rotation.x)
            modified_object.rotation_euler[1] += math.radians(cur_rotation.y)
            modified_object.rotation_euler[2] += math.radians(cur_rotation.z)
            Engine.global_rotation = mathutils.Vector((0.0, 0.0, 0.0))
            
            # Scale each object to the predecessor's scale + global engine scale
            cur_scale = modified_object.scale
            cur_scale.x *= Engine.global_scale.x
            cur_scale.y *= Engine.global_scale.y
            cur_scale.z *= Engine.global_scale.z
            modified_object.scale = cur_scale
            Engine.global_scale = mathutils.Vector((1, 1, 1))

    # Closure for Operators.add_material
    @staticmethod
    def apply_texture(tex_path):
        def apply_texture_func():
            return Operators.apply_texture(tex_path)
        return apply_texture_func
            
    # Closure for Operators.uv_project_cube
    @staticmethod
    def uv_project_cube():
        return Operators.uv_project_cube
    
    # Alias for Operators.get_current
    @staticmethod
    def get_current():
        return Operators.get_current
    
    # Closure for Operators.place_plane
    @staticmethod
    def place_plane(position):
        def place_plane_func():
            return Operators.place_plane(position)
        return place_plane_func
    
    # Closure for Operators.remove
    @staticmethod
    def remove():
        return Operators.remove
    
    # Closure for Operators.place_mesh
    @staticmethod
    def place_mesh(file):
        def place_mesh_func():
            return Operators.place_mesh(file)
        return place_mesh_func
    
    # Closure for Operators.scale
    @staticmethod
    def scale(x, y, z):
        def scale_func():
            return Operators.scale(x, y, z)
        return scale_func
    
    # Closure for Operators.translate
    @staticmethod
    def translate(amts):
        def translate_func():
            return Operators.translate(amts)
        return translate_func

    # Closure for Operators.rotate
    @staticmethod
    def rotate(axis, amt):
        def rotate_func():
            return Operators.rotate(axis, amt)
        return rotate_func

    # Closure for Operators.extrude
    @staticmethod
    def extrude(amt):
        def extrude_func():
            return Operators.extrude(amt)
        return extrude_func

    # Closure for Operators.component_extrude
    @staticmethod
    def component_extrude(axis, amt):
        def extrude_func():
            return Operators.component_extrude(axis, amt)
        return extrude_func