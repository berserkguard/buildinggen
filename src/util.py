import bpy
import mathutils

from buildinggen.constants import *

class Util:
    logging_enabled = True

    @staticmethod
    def add_material(name, texture_path):
        texture = bpy.data.textures.new(name + "_tex", type="IMAGE")
        texture.image = bpy.data.images.load(texture_path)

        mat = bpy.data.materials.new(name)
        mtex = mat.texture_slots.add()
        mtex.texture = texture
        mtex.texture_coords = "UV"
        
        """
        mat.diffuse_color = (1, 1, 1)
        mat.diffuse_shader = 'LAMBERT' 
        mat.diffuse_intensity = 1.0
        mat.specular_color = (1, 1, 1)
        mat.specular_shader = 'COOKTORR'
        mat.specular_intensity = 0.5
        mat.alpha = 1.0
        mat.ambient = 1
        """
        return mat

    # Returns a list of all children (recursively) of the specified object.
    @staticmethod
    def get_children(object):
        children = []
        
        for child in object.children:
            children.append(child)
            children += Util.get_children(child)
        
        return children
    
    # Returns true if the given name matches the label. For example, both 'Base' and 'Base.001' match label 'Base'.
    @staticmethod
    def name_matches(name, label):
        arr = name.rsplit(".", 1)
        return arr[0] == label
    
    @staticmethod
    def get_added_objects(old_list, new_list):
        # Make a shallow copy of list so we don't mess things up!
        added_objects = list(new_list)
        
        for i in old_list:
            added_objects.remove(i)

        return added_objects
    
    @staticmethod
    def separate_object():
        lo_b = [ob for ob in bpy.data.objects if ob.type == 'MESH']
        bpy.ops.mesh.separate(type='SELECTED')
        lo_a = [ob for ob in bpy.data.objects if ob.type == 'MESH']
        
        for i in lo_b:
            lo_a.remove(i)
        separate_object = lo_a[0]
        return separate_object
    
    @staticmethod
    def get_common_normal():
        normal = mathutils.Vector((0.0, 0.0, 0.0))
        object = bpy.context.scene.objects.active
        if object.type == 'MESH':
            for poly in object.data.polygons:
                normal.xyz += poly.normal.xyz

        return normal.normalized()
    
    @staticmethod
    def log(msg):
        if Util.logging_enabled:
            print("[BuildingGen]", msg)
    
    @staticmethod
    def add_subfolder_to_path(folder):
        paths = os.path.split(inspect.getfile(inspect.currentframe()));
        
        cmd_subfolder = os.path.realpath(os.path.abspath(os.path.join(paths[0], folder)))
        if cmd_subfolder not in sys.path:
            sys.path.insert(0, cmd_subfolder)
    
    @staticmethod
    def set_metadata(object, key, value):
        key = Constants.METADATA_PREFIX + key
        object[key] = value
    
    @staticmethod
    def get_metadata(object, key, default = None):
        full_key = Constants.METADATA_PREFIX + key
        
        # If the key exists in the object, simply return the associated value.
        if full_key in object:
            return object[full_key]
            
        # Otherwise, if we have a parent, try to get the metadata from it.
        if object.parent is not None:
            return Util.get_metadata(object.parent, key, default)
        return default
    
    # object_mode can be either 'EDIT' or 'OBJECT'
    @staticmethod
    def setup_operation(object_mode):
        bpy.ops.object.mode_set(mode=object_mode, toggle=False)
        
        # Unselect all objects, except for the current one.
        for ob in bpy.context.scene.objects:
            if ob.type == 'MESH':
                ob.select = False
        
        object = bpy.context.scene.objects.active
        object.select = True
    
    # Sets the rendering up to be shadeless & use face texture.
    @staticmethod
    def render_setup():
        for ob in bpy.context.scene.objects:
            for slot in ob.material_slots:
                if slot.material is not None:
                    slot.material.use_shadeless = True
                    slot.material.use_face_texture = True
    