# Blender add-on information.
bl_info = {
    "name": "BuildingGen",
    "author": "Ryan Norby",
    "version": (1, 0),
    "blender": (2, 63, 0),
    "description": "Procedurally generate buildings and other objects.",
    "category": "Object"
}

# System imports
import bpy
import mathutils
import sys
import types
import os

# Local imports
from buildinggen.building_gen import *
from buildinggen.ui import *
from buildinggen.command_options import *

# Needed by Blender to register the add-on
def register():
    bpy.utils.register_class(BuildingGen)
    bpy.utils.register_class(RulesetChooserOperator)
    bpy.utils.register_class(RulesetConfigOperator)
    Util.log("Registered!")

# Needed by Blender to unregister the add-on
def unregister():
    bpy.utils.unregister_class(RulesetConfigOperator)
    bpy.utils.unregister_class(RulesetChooserOperator)
    bpy.utils.unregister_class(BuildingGen)
    Util.log("Unregistered!")
   
# This allows you to run the script directly from blenders text editor
# to test the addon without having to install it.
if __name__ == "__main__":
    try:
        register()
    except ValueError:
        pass
    
    # Setup the custom script arguments.
    if CommandOptions.setup_args(sys.argv):
        # Get the specified ruleset from the command options.
        ruleset_name = CommandOptions.values["-r"][0]
        
        # Get the specified output file from the command options.
        output_file = CommandOptions.values["-o"][0]
        
        # Make sure the ruleset provided is valid.
        if not os.path.isfile(os.path.join(Constants.RULESET_PATH, ruleset_name + ".py")):
            Util.log("Error! Ruleset not found: " + ruleset_name)
        else:
            # Arguments provided; use custom ruleset.
            BuildingGen.ruleset_module = ruleset_name
            BuildingGen.ruleset_name = ruleset_name
            
            # Load the ruleset.
            BuildingGen.load_ruleset(BuildingGen.ruleset_module)

            # Execute the add-on
            bpy.ops.object.building_gen();
            
            # Save the resulting Blender file.
            bpy.ops.wm.save_as_mainfile(filepath=output_file)
