import bpy
import mathutils
import os

from buildinggen.building_gen import *
from buildinggen.constants import *
from buildinggen.engine import *

# Pop-up dialog for choosing the ruleset to use.
class RulesetChooserOperator(bpy.types.Operator):
    bl_idname = "object.buildinggen_ruleset_chooser"
    bl_label = "BuildingGen - Choose Ruleset"
        
    # The most recently selected ruleset.
    cur_ruleset = None
        
    def get_items(scene, context):
        # Get list of files in the rulesets path so we can build the EnumProperty items list.
        ruleset_names = [os.path.splitext(filename)[0] for filename in next(os.walk(Constants.RULESET_PATH))[2]]
        
        enum_items = []
        for ruleset in ruleset_names:
            enum_items.append((ruleset, ruleset, ""))
        
        # Need to set cur_ruleset to the first item in the list, because enum_chosen is only called IF they change the value.
        RulesetChooserOperator.cur_ruleset = ruleset_names[0]
        
        return enum_items
    
    def enum_chosen(self, context):
        #print(self.ruleset)
        RulesetChooserOperator.cur_ruleset = self.ruleset
    
    # The ruleset selector property.
    ruleset = bpy.props.EnumProperty(name="Ruleset", description="Choose a ruleset!", items=get_items, update=enum_chosen)
    
    def execute(self, context):
        #Util.log("Ruleset Dialog ran!")
        print(RulesetChooserOperator.cur_ruleset)
        BuildingGen.ruleset_module = RulesetChooserOperator.cur_ruleset
        
        # Load the ruleset.
        BuildingGen.load_ruleset(BuildingGen.ruleset_module)
        
        # Apply the ruleset if applicable.
        if BuildingGen.apply_after_set:
            BuildingGen.apply_after_set = False
            BuildingGen.apply_ruleset()
        
        return {'FINISHED'}

    def invoke(self, context, event):
        return context.window_manager.invoke_props_dialog(self)
    
    
def get_var_prop(vars, idx, sub_idx):
    if idx >= len(vars):
        return None
    return vars[idx].get_prop(sub_idx)
    
# Pop-up dialog for configuring the current ruleset.
class RulesetConfigOperator(bpy.types.Operator):
    bl_idname = "object.buildinggen_ruleset_config"
    bl_label = "BuildingGen - Configure Ruleset"
    
    def __init__(self):
        self.vars = []
        for id, var in Engine.vars.vars.items():
            self.vars.append(var)
        
        # Need to sort by index to get desired render order
        self.vars = sorted(self.vars, key=lambda v: v.index)
        
        # Can't dynamically add custom properties because Blender is stupid >_>
        # Also, since we need a property for both min AND max (for a single integer variable),
        # we need to use sub-indices. Ugly, but necessary.
        bpy.types.Scene.ruleset_var_00_0 = get_var_prop(self.vars, 0, 0);        bpy.types.Scene.ruleset_var_00_1 = get_var_prop(self.vars, 0, 1)
        bpy.types.Scene.ruleset_var_01_0 = get_var_prop(self.vars, 1, 0);        bpy.types.Scene.ruleset_var_01_1 = get_var_prop(self.vars, 1, 1)
        bpy.types.Scene.ruleset_var_02_0 = get_var_prop(self.vars, 2, 0);        bpy.types.Scene.ruleset_var_02_1 = get_var_prop(self.vars, 2, 1)
        bpy.types.Scene.ruleset_var_03_0 = get_var_prop(self.vars, 3, 0);        bpy.types.Scene.ruleset_var_03_1 = get_var_prop(self.vars, 3, 1)
        bpy.types.Scene.ruleset_var_04_0 = get_var_prop(self.vars, 4, 0);        bpy.types.Scene.ruleset_var_04_1 = get_var_prop(self.vars, 4, 1)
        bpy.types.Scene.ruleset_var_05_0 = get_var_prop(self.vars, 5, 0);        bpy.types.Scene.ruleset_var_05_1 = get_var_prop(self.vars, 5, 1)
        bpy.types.Scene.ruleset_var_06_0 = get_var_prop(self.vars, 6, 0);        bpy.types.Scene.ruleset_var_06_1 = get_var_prop(self.vars, 6, 1)
        bpy.types.Scene.ruleset_var_07_0 = get_var_prop(self.vars, 7, 0);        bpy.types.Scene.ruleset_var_07_1 = get_var_prop(self.vars, 7, 1)
        bpy.types.Scene.ruleset_var_08_0 = get_var_prop(self.vars, 8, 0);        bpy.types.Scene.ruleset_var_08_1 = get_var_prop(self.vars, 8, 1)
        bpy.types.Scene.ruleset_var_09_0 = get_var_prop(self.vars, 9, 0);        bpy.types.Scene.ruleset_var_09_1 = get_var_prop(self.vars, 9, 1)

        bpy.types.Scene.ruleset_var_10_0 = get_var_prop(self.vars, 10, 0);       bpy.types.Scene.ruleset_var_10_1 = get_var_prop(self.vars, 10, 1)
        bpy.types.Scene.ruleset_var_11_0 = get_var_prop(self.vars, 11, 0);       bpy.types.Scene.ruleset_var_11_1 = get_var_prop(self.vars, 11, 1)
        bpy.types.Scene.ruleset_var_12_0 = get_var_prop(self.vars, 12, 0);       bpy.types.Scene.ruleset_var_12_1 = get_var_prop(self.vars, 12, 1)
        bpy.types.Scene.ruleset_var_13_0 = get_var_prop(self.vars, 13, 0);       bpy.types.Scene.ruleset_var_13_1 = get_var_prop(self.vars, 13, 1)
        bpy.types.Scene.ruleset_var_14_0 = get_var_prop(self.vars, 14, 0);       bpy.types.Scene.ruleset_var_14_1 = get_var_prop(self.vars, 14, 1)
        bpy.types.Scene.ruleset_var_15_0 = get_var_prop(self.vars, 15, 0);       bpy.types.Scene.ruleset_var_15_1 = get_var_prop(self.vars, 15, 1)
        bpy.types.Scene.ruleset_var_16_0 = get_var_prop(self.vars, 16, 0);       bpy.types.Scene.ruleset_var_16_1 = get_var_prop(self.vars, 16, 1)
        bpy.types.Scene.ruleset_var_17_0 = get_var_prop(self.vars, 17, 0);       bpy.types.Scene.ruleset_var_17_1 = get_var_prop(self.vars, 17, 1)
        bpy.types.Scene.ruleset_var_18_0 = get_var_prop(self.vars, 18, 0);       bpy.types.Scene.ruleset_var_18_1 = get_var_prop(self.vars, 18, 1)
        bpy.types.Scene.ruleset_var_19_0 = get_var_prop(self.vars, 19, 0);       bpy.types.Scene.ruleset_var_19_1 = get_var_prop(self.vars, 19, 1)
    
    def execute(self, context):
        Util.log("Ruleset Dialog ran!")
        
        # Clean up each of the variables. This does constraint-enforcing (for example, making sure min <= max).
        Engine.vars.clean_up_all()
        
        return {'FINISHED'}

    def invoke(self, context, event):
        return context.window_manager.invoke_props_dialog(self)

    def draw(self, context):
        # At least we can loop here...
        for i in range(20):
            first_prop = get_var_prop(self.vars, i, 0)
            second_prop = get_var_prop(self.vars, i, 1)
            
            if first_prop is not None:
                row = self.layout.row()
                row.prop(bpy.context.scene, "ruleset_var_" + ('%02d' % i) + "_0", slider=True)
                if second_prop is not None:
                    row.prop(bpy.context.scene, "ruleset_var_" + ('%02d' % i) + "_1", slider=True)
