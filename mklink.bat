@echo off
REM This script creates a Windows symbolic link from the module's src/ directory and a directory within the Blender's addon path.

SETLOCAL

REM The path to the "addons" folder that Blender loads at start-up.
set BLENDER_ADDON_PATH="C:\Program Files\Blender Foundation\Blender\2.69\scripts\addons\"

REM The name of the addon folder to symlink to. You should leave this as it is.
set ADDON_NAME="buildinggen"

mklink /j %BLENDER_ADDON_PATH%%ADDON_NAME% "src"

PAUSE