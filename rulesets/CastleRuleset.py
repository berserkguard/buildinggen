from buildinggen.engine import *
from buildinggen.ruleset import *
from buildinggen.util import *

import random

class CastleRuleset(Ruleset):
    def get_name(self):
        return "CastleRuleset"
    
    def __init__(self):
        Engine.vars.new_integer("Castle Width", min=16, max=24)
        Engine.vars.new_integer("Castle Height", min=10, max=16)
    
    def apply(self):
        # Variables
        CASTLE_WIDTH = Engine.vars.get("Castle Width")
        CASTLE_HEIGHT = Engine.vars.get("Castle Height")
        
        # First, we need to set the axiom.
        Engine.set_axiom("Base", Engine.place_plane((0, 0, 0)))
        
        # Step 1: Create floor of castle, and texture with grass.
        Engine.apply("Base", "Floor", Engine.scale(CASTLE_WIDTH/2, CASTLE_HEIGHT/2, 1.0))
        Engine.apply("Floor", "Floor", Engine.uv_project_cube())
        Engine.apply("Floor", "Floor", Engine.apply_texture(os.path.join("models", "textures", "grass.jpg")))
        
        # Step 2: Create the round towers at each of the castle's corners
        Engine.apply_compound("Floor", [
            Engine.queue_translate(-CASTLE_WIDTH/2, -CASTLE_HEIGHT/2, 0.0),
            Engine.queue_rotate(0.0, 0.0, 90.0),
            Engine.queue_set_metadata("direction", "horizontal"),
            ["CastleTower", Engine.place_mesh("models\\castle_tower_round.obj")],
            
            Engine.queue_translate(-CASTLE_WIDTH/2, CASTLE_HEIGHT/2, 0.0),
            Engine.queue_rotate(0.0, 0.0, 0.0),
            Engine.queue_set_metadata("direction", "vertical"),
            ["CastleTower", Engine.place_mesh("models\\castle_tower_round.obj")],
            
            Engine.queue_translate(CASTLE_WIDTH/2, -CASTLE_HEIGHT/2, 0.0),
            Engine.queue_rotate(0.0, 0.0, 180.0),
            Engine.queue_set_metadata("direction", "vertical"),
            ["CastleTower", Engine.place_mesh("models\\castle_tower_round.obj")],
            
            Engine.queue_translate(CASTLE_WIDTH/2, CASTLE_HEIGHT/2, 0.0),
            Engine.queue_rotate(0.0, 0.0, 270.0),
            Engine.queue_set_metadata("direction", "horizontal"),
            ["CastleTower", Engine.place_mesh("models\\castle_tower_round.obj")],
            
            Engine.queue_clear_metadata("direction"),
        ])
        
        # Step 3: For each corner tower, place a wall.
        Engine.apply("CastleTower", "CastleWall", Engine.place_mesh("models\\castle_wall.obj"))
        
        # Step 4: Scale the horizontal walls to span between the corner towers.
        def horizontal_wall(object):
            return Util.get_metadata(object, "direction") == "horizontal"
        
        Engine.apply("CastleWall", "CastleWallScaled", Engine.scale(CASTLE_WIDTH, 1.0, 1.0), horizontal_wall)

        # Step 5: Scale the vertical walls to span between the corner towers.
        def vertical_wall(object):
            return Util.get_metadata(object, "direction") == "vertical"
        
        Engine.apply("CastleWall", "CastleWallScaled", Engine.scale(1.0, CASTLE_HEIGHT, 1.0), vertical_wall)

        # Step 5b: Cube project on scaled walls
        Engine.apply("CastleWallScaled", "CastleWallScaled", Engine.uv_project_cube())
        
        # Step 6: Randomly place portcullis tower(s)
        def wall_has_portcullis(object):
            long_enough = (object.dimensions.y >= 8.0)
            is_horizontal = (Util.get_metadata(object, "direction") == "horizontal")
            makes_cut = (random.randint(1,2) == 1)
            
            return long_enough and is_horizontal and makes_cut
        
        Engine.apply_compound("CastleWallScaled", [
            Engine.queue_translate(0.0, -CASTLE_WIDTH / 2, 0.0),
            Engine.queue_rotate(0.0, 0.0, 90.0),
            ["MidTower", Engine.place_mesh("models\\castle_tower_portcullis.obj")]
        ], wall_has_portcullis)
        