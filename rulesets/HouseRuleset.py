from buildinggen.engine import *
from buildinggen.ruleset import *
from buildinggen.util import *

import random

class HouseRuleset(Ruleset):
    def get_name(self):
        return "HouseRuleset"
    
    def __init__(self):
        Engine.vars.new_integer("House Width", min=4, max=7)
        Engine.vars.new_integer("House Height", min=4, max=6)
        Engine.vars.new_integer("Num Floors", min=1, max=3)
        Engine.vars.new_integer("Floor Height", val=3, min=2, max=4)
        Engine.vars.new_enum("Roof Type", [
            ("Random", "Randomly selects one of the below roof types."),
            ("Steepled", "A steepled roof."),
            ("Sloped", "A sloped roof."),
            ("Gabled", "A gabled roof."),
            ("Hipped", "A hipped roof.")
        ])
        Engine.vars.new_enum("Roof Texture", [
            ("Random", "Randomly selects one of the below roof textures."),
            ("Shingled", "A gray shingled roof."),
            ("Clay Tile", "An aqua clay tile roof."),
            ("Thatched", "A thatched roof."),
            ("Rolled", "A rolled roof.")
        ])
        Engine.vars.new_enum("Wall Texture", [
            ("Random", "Randomly selects one of the below wall textures."),
            ("Worn Siding", "A worn wall with beige siding."),
            ("Red Siding", "A wall with red siding."),
            ("Stone Wall", "A stone wall."),
            ("Pitted", "A pitted stucco wall."),
            ("Log", "A cabin-style log wall.")
        ])
        Engine.vars.new_bool("Chimney", val=True)
        Engine.vars.new_bool("Stairs", val=True)

    def apply(self):
        # Variables
        HOUSE_WIDTH = Engine.vars.get("House Width")
        HOUSE_HEIGHT = Engine.vars.get("House Height")
        NUM_FLOORS = Engine.vars.get("Num Floors")
        FLOOR_HEIGHT = Engine.vars.get("Floor Height")
        ROOF_TYPE = Engine.vars.get("Roof Type")
        ROOF_TEX = Engine.vars.get("Roof Texture")
        WALL_TEX = Engine.vars.get("Wall Texture")
        HAS_CHIMNEY = Engine.vars.get("Chimney")
        CHIMNEY_SIDE = random.randint(0, 1)
        HAS_STAIRS = Engine.vars.get("Stairs")
        
        roof_dict = {
            "Steepled": "models\\house_roof_steepled.obj",
            "Sloped": "models\\house_roof_sloped.obj",
            "Gabled": "models\\house_roof_gabled.obj",
            "Hipped": "models\\house_roof_hipped.obj"
        }
        roof_tex_dict = {
            "Shingled": "models\\textures\\shingles.jpg",
            "Clay Tile": "models\\textures\\shingles_2.jpg",
            "Thatched": "models\\textures\\thatched_roof.jpg",
            "Rolled": "models\\textures\\rolled_roof.jpg"
        }
        wall_dict = {
            "Worn Siding": "models\\textures\\siding.jpg",
            "Red Siding": "models\\textures\\red_siding.jpg",
            "Stone Wall": "models\\textures\\stone_wall.jpg",
            "Pitted": "models\\textures\\pitted_wall.jpg",
            "Log": "models\\textures\\log_wall.jpg"
        }
        
        # Pick a random roof if not already set.
        if ROOF_TYPE == "Random":
            ROOF_TYPE = random.choice(list(roof_dict.values()))
        else:
            ROOF_TYPE = roof_dict[ROOF_TYPE]
            
        # Pick a random roof texture if not already set.
        if ROOF_TEX == "Random":
            ROOF_TEX = random.choice(list(roof_tex_dict.values()))
        else:
            ROOF_TEX = roof_tex_dict[ROOF_TEX]
        
        # Pick a random wall texture if not already set.
        if WALL_TEX == "Random":
            WALL_TEX = random.choice(list(wall_dict.values()))
        else:
            WALL_TEX = wall_dict[WALL_TEX]
        
        # First, we need to set the axiom.
        Engine.set_axiom("Base", Engine.place_plane((0, 0, 0)))
        
        # Step 1: Create floor of house
        Engine.apply("Base", "Floor", Engine.scale(HOUSE_WIDTH/2, HOUSE_HEIGHT/2, 1.0))
        
        # Step 2: Create the levels
        for i in range(NUM_FLOORS):
            Engine.apply("Floor", ["Floor" + str(i), "Walls", "Floor"], Engine.component_extrude('Z', FLOOR_HEIGHT))
        
        Engine.apply("Walls", "Walls", Engine.uv_project_cube())
        Engine.apply("Walls", "Walls", Engine.apply_texture(WALL_TEX))

        # Now, add a roof.
        Engine.apply_compound("Floor", [
            ["Ceiling", Engine.get_current()],
            Engine.queue_translate(0, 0, FLOOR_HEIGHT * NUM_FLOORS),
            Engine.queue_scale(HOUSE_WIDTH/2, HOUSE_HEIGHT/2, 1.0),
            ["Roof", Engine.place_mesh(ROOF_TYPE)],
        ])
        Engine.apply("Roof", "Roof", Engine.apply_texture(ROOF_TEX))
        
        # Add a chimney (if specified)
        def has_chimney(object):
            return HAS_CHIMNEY
        
        Engine.apply_compound("Floor0", [
            Engine.queue_translate(-HOUSE_WIDTH/2, random.uniform(-HOUSE_HEIGHT/3, HOUSE_HEIGHT/3), 0),
            Engine.queue_scale(1.0 + (NUM_FLOORS - 1) * 0.25, 1.0 + (NUM_FLOORS - 1) * 0.25, 0.75 + (NUM_FLOORS - 1) * 0.5),
            Engine.queue_rotate(0.0, 0.0, 180.0 * CHIMNEY_SIDE),
            Engine.queue_translate(HOUSE_WIDTH * CHIMNEY_SIDE, 0.0, 0.0),
            ["Chimney", Engine.place_mesh("models\\house_fireplace.obj")],
        ], has_chimney)
        
        # Add front steps (if specified)
        def has_stairs(object):
            return HAS_STAIRS
        
        stair_width = random.uniform(0.5, 0.8)
        Engine.apply_compound("Floor0", [
            Engine.queue_translate(random.uniform(-HOUSE_WIDTH/3, HOUSE_WIDTH/3), HOUSE_HEIGHT/2, 0),
            Engine.queue_scale(0.3, stair_width, 0.3),
            Engine.queue_rotate(0.0, 0.0, 270.0),
            ["Steps", Engine.place_mesh("models\\house_steps.obj")],
        ], has_stairs)
        
        Engine.apply("Steps", "Steps", Engine.uv_project_cube())
        
        # Add a front door
        Engine.apply_compound("Steps", [
            Engine.queue_translate(0, 0, 0.45/0.3),
            Engine.queue_scale(1 / 0.3, 1 / stair_width, 1 / 0.3),
            ["Door", Engine.place_mesh("models\\house_door.obj")]
        ])
        