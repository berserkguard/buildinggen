from buildinggen.engine import *
from buildinggen.ruleset import *
from buildinggen.util import *

import random

class TableRuleset(Ruleset):
    def get_name(self):
        return "TableRuleset"
    
    def __init__(self):
        Engine.vars.new_integer("Table Width", min=5, max=8)
        Engine.vars.new_integer("Table Height", min=3, max=5)
        Engine.vars.new_enum("Texture", [
            ("Random", "Randomly selects one of the below textures."),
            ("Charred", "A charred table."),
            ("Marble", "A marble table."),
            ("Leather", "A leather table."),
            ("Wood", "A wood table."),
            ("Palm", "A palm table.")
        ])
        Engine.vars.new_enum("Leg Type", [
            ("Random", "Randomly selects one of the below leg types."),
            ("Square", "A square leg."),
            ("Curly", "A curly leg."),
            ("Tapered", "A tapered leg.")
        ])
        Engine.vars.new_enum("Edge Type", [
            ("Random", "Randomly selects one of the below edge types."),
            ("None", "No edge."),
            ("Rounded", "A rounded edge.")
        ])

    def apply(self):
        # Variables
        TABLE_WIDTH = Engine.vars.get("Table Width")
        TABLE_HEIGHT = Engine.vars.get("Table Height")
        TEX = Engine.vars.get("Texture")
        LEG_TYPE = Engine.vars.get("Leg Type")
        EDGE_TYPE = Engine.vars.get("Edge Type")
        CORNER_TYPE = None
        
        tex_dict = {
            "Charred": "models\\textures\\charred_wood.jpg",
            "Marble": "models\\textures\\marble.jpg",
            "Leather": "models\\textures\\leather.jpg",
            "Wood": "models\\textures\\bark.jpg",
            "Palm": "models\\textures\\palm.jpg"
        }
        leg_dict = {
            "Square": "models\\table_leg_square.obj",
            "Curly": "models\\table_leg_curly.obj",
            "Tapered": "models\\table_leg_tapered.obj"
        }
        edge_dict = {
            "None": (None, None),
            "Rounded": ("models\\table_edge_rounded.obj", "models\\table_corner_rounded.obj")
        }
        
        # Pick a random texture if not already set.
        if TEX == "Random":
            TEX = random.choice(list(tex_dict.values()))
        else:
            TEX = tex_dict[TEX]
        
        # Pick a random leg type if not already set.
        if LEG_TYPE == "Random":
            LEG_TYPE = random.choice(list(leg_dict.values()))
        else:
            LEG_TYPE = leg_dict[LEG_TYPE]
        
        # Pick a random edge type if not already set.
        if EDGE_TYPE == "Random":
            val = random.choice(list(edge_dict.values()))
            EDGE_TYPE = val[0]
            CORNER_TYPE = val[1]
        else:
            val = edge_dict[EDGE_TYPE]
            EDGE_TYPE = val[0]
            CORNER_TYPE = val[1]
        
        # First, we need to set the axiom.
        Engine.set_axiom("Base", Engine.place_mesh("models\\table_surface.obj"))
        
        # Step 1: Create surface of table
        Engine.apply("Base", "Top", Engine.scale(TABLE_WIDTH, TABLE_HEIGHT, 1.0))

        # Step 2: Add the legs and cross-bars
        Engine.apply_compound("Top", [
            Engine.queue_rotate(0.0, 0.0, 270.0),
            Engine.queue_translate(-TABLE_WIDTH/2 + 0.5, -TABLE_HEIGHT/2 + 0.5, 0.0),
            ["Leg", Engine.place_mesh(LEG_TYPE)],
            Engine.queue_rotate(0.0, 0.0, 90.0),
            Engine.queue_translate(-TABLE_WIDTH/2 + 0.5, -TABLE_HEIGHT/2 + 0.5, 0.0),
            Engine.queue_set_metadata("direction", "horizontal"),
            ["Bar", Engine.place_mesh("models\\table_bar_square.obj")],
            
            Engine.queue_rotate(0.0, 0.0, 270.0),
            Engine.queue_translate(-TABLE_WIDTH/2 + 0.5, TABLE_HEIGHT/2 - 0.5, 0.0),
            ["Leg", Engine.place_mesh(LEG_TYPE)],
            Engine.queue_rotate(0.0, 0.0, 0.0),
            Engine.queue_translate(-TABLE_WIDTH/2 + 0.5, TABLE_HEIGHT/2 - 0.5, 0.0),
            Engine.queue_set_metadata("direction", "vertical"),
            ["Bar", Engine.place_mesh("models\\table_bar_square.obj")],
            
            Engine.queue_rotate(0.0, 0.0, 90.0),
            Engine.queue_translate(TABLE_WIDTH/2 - 0.5, -TABLE_HEIGHT/2 + 0.5, 0.0),
            ["Leg", Engine.place_mesh(LEG_TYPE)],
            Engine.queue_rotate(0.0, 0.0, 180.0),
            Engine.queue_translate(TABLE_WIDTH/2 - 0.5, -TABLE_HEIGHT/2 + 0.5, 0.0),
            Engine.queue_set_metadata("direction", "vertical"),
            ["Bar", Engine.place_mesh("models\\table_bar_square.obj")],
            
            Engine.queue_rotate(0.0, 0.0, 90.0),
            Engine.queue_translate(TABLE_WIDTH/2 - 0.5, TABLE_HEIGHT/2 - 0.5, 0.0),
            ["Leg", Engine.place_mesh(LEG_TYPE)],
            Engine.queue_rotate(0.0, 0.0, 270.0),
            Engine.queue_translate(TABLE_WIDTH/2 - 0.5, TABLE_HEIGHT/2 - 0.5, 0.0),
            Engine.queue_set_metadata("direction", "horizontal"),
            ["Bar", Engine.place_mesh("models\\table_bar_square.obj")],
            
            Engine.queue_clear_metadata("direction"),
        ])
        
        # Step 3: Add the edges (if specified)
        def has_edge(object):
            return EDGE_TYPE is not None
        Engine.apply_compound("Top", [
            Engine.queue_rotate(0.0, 0.0, 90.0),
            Engine.queue_translate(0.0, -TABLE_HEIGHT/2, 0.0),
            Engine.queue_set_metadata("direction", "horizontal"),
            ["Edge", Engine.place_mesh(EDGE_TYPE)],
            Engine.queue_rotate(0.0, 0.0, 90.0),
            Engine.queue_translate(-TABLE_WIDTH/2, -TABLE_HEIGHT/2, 0.0),
            ["Corner", Engine.place_mesh(CORNER_TYPE)],
            
            Engine.queue_rotate(0.0, 0.0, 0.0),
            Engine.queue_translate(-TABLE_WIDTH/2, 0.0, 0.0),
            Engine.queue_set_metadata("direction", "vertical"),
            ["Edge", Engine.place_mesh(EDGE_TYPE)],
            Engine.queue_rotate(0.0, 0.0, 0.0),
            Engine.queue_translate(-TABLE_WIDTH/2, TABLE_HEIGHT/2, 0.0),
            ["Corner", Engine.place_mesh(CORNER_TYPE)],

            Engine.queue_rotate(0.0, 0.0, 180.0),
            Engine.queue_translate(TABLE_WIDTH/2, 0.0, 0.0),
            Engine.queue_set_metadata("direction", "vertical"),
            ["Edge", Engine.place_mesh(EDGE_TYPE)],
            Engine.queue_rotate(0.0, 0.0, 180.0),
            Engine.queue_translate(TABLE_WIDTH/2, -TABLE_HEIGHT/2, 0.0),
            ["Corner", Engine.place_mesh(CORNER_TYPE)],

            Engine.queue_rotate(0.0, 0.0, 270.0),
            Engine.queue_translate(0.0, TABLE_HEIGHT/2, 0.0),
            Engine.queue_set_metadata("direction", "horizontal"),
            ["Edge", Engine.place_mesh(EDGE_TYPE)],
            Engine.queue_rotate(0.0, 0.0, 270.0),
            Engine.queue_translate(TABLE_WIDTH/2, TABLE_HEIGHT/2, 0.0),
            ["Corner", Engine.place_mesh(CORNER_TYPE)],
            
            Engine.queue_clear_metadata("direction"),
        ], has_edge)
        
        # Step 4: Scale the cross-bars and edges
        def horizontal_bar(object):
            return Util.get_metadata(object, "direction") == "horizontal"
        Engine.apply("Bar", "Bar", Engine.scale(TABLE_WIDTH - 1, 1.0, 1.0), horizontal_bar)
        Engine.apply("Edge", "Edge", Engine.scale(TABLE_WIDTH, 1.0, 1.0), horizontal_bar)

        def vertical_bar(object):
            return Util.get_metadata(object, "direction") == "vertical"
        Engine.apply("Bar", "Bar", Engine.scale(1.0, TABLE_HEIGHT - 1, 1.0), vertical_bar)
        Engine.apply("Edge", "Edge", Engine.scale(1.0, TABLE_HEIGHT, 1.0), vertical_bar)
        
        # Step 5: Recalculate UVs & apply textures
        Engine.apply("Top", "Top", Engine.uv_project_cube())
        Engine.apply("Top", "Top", Engine.apply_texture(TEX))
        Engine.apply("Edge", "Edge", Engine.uv_project_cube())
        Engine.apply("Edge", "Edge", Engine.apply_texture(TEX))
        Engine.apply("Bar", "Bar", Engine.uv_project_cube())
        Engine.apply("Bar", "Bar", Engine.apply_texture("models\\textures\\wood.jpg"))
        Engine.apply("Corner", "Corner", Engine.uv_project_cube())
        Engine.apply("Corner", "Corner", Engine.apply_texture(TEX))
        Engine.apply("Leg", "Leg", Engine.uv_project_cube())
        Engine.apply("Leg", "Leg", Engine.apply_texture("models\\textures\\wood.jpg"))
