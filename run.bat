@echo off

REM Note: Blender must be in your PATH variable for this script to work.

SETLOCAL
SET ruleset_name=HouseRuleset
SET output_file=output.blend

REM blender.exe -b --enable-autoexec test.blend -P src/__init__.py -- --help
blender.exe -b --enable-autoexec test.blend -P src/__init__.py -- -r %ruleset_name% -o %output_file%

PAUSE